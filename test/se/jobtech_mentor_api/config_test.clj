(ns se.jobtech-mentor-api.config-test
  (:require [clojure.test :refer [deftest is testing]]
            [se.jobtech-mentor-api.config :as config]))

(deftest test-get-config
  (let [config (config/get-config [])]
    (testing "test get config"
      (is (>= (count config) 7))
      (is (some? config))
      (is (= {:port 3000
              :git-login "mentor-taxonomy"
              :git-password "<SET AS ENV VARIABLE>"
              :api-private-key "use-env-variable-for-production"
              :jobtech-taxonomy-api-key "use-env-variable-for-production"
              :default-git-repo-path "./tmp/annotations"
              :path "resources/mentor-api.edn"} config)))))
