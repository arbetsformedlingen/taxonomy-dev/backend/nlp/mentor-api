(ns se.jobtech-mentor-api.generate-data-functions-test
  (:require [clojure.test :refer [deftest is testing]]
            [peridot.multipart]
            [se.jobtech-mentor-api.generate-data-functions :as gdf]))

(deftest test-merge-datas
  (testing "Test merge datas"
    (let [result (gdf/merge-datas [{:alternative_labels [],
                                    :id "115i_88X_d9i",
                                    :preferred_label "underhålla tandläkarmottagningens behandlingsenhet",
                                    :type "esco-skill",
                                    :uri "http://data.jobtechdev.se/taxonomy/concept/115i_88X_d9i"}]
                                  [#:taxonomy{:event-type "DEPRECATED",
                                              :user-id "admin-2",
                                              :latest-version-of-concept #:taxonomy{:id "wwEt_ceM_Uj8",
                                                                                    :type "occupation-name",
                                                                                    :definition "Tapetvalsgravör",
                                                                                    :preferred-label "Tapetvalsgravör"},
                                              :version -1, :comment "Aktuella annonser saknas. Yrkesbenämning hänvisas inte. Uppdatering enligt egen utredning."}])]
      (is (= [{:alternative_labels [],
               :id "115i_88X_d9i",
               :preferred_label "underhålla tandläkarmottagningens behandlingsenhet",
               :type "esco-skill",
               :uri "http://data.jobtechdev.se/taxonomy/concept/115i_88X_d9i"}
              {:definition "Tapetvalsgravör",
               :id "wwEt_ceM_Uj8",
               :preferred-label "Tapetvalsgravör",
               :type "occupation-name"}] result)))))

(deftest test-extract-latest-version
  (testing "Test extract latest version"
    (let [result (->> [#:taxonomy{:event-type "DEPRECATED",
                                  :user-id "admin-2",
                                  :latest-version-of-concept #:taxonomy{:id "wwEt_ceM_Uj8",
                                                                        :type "occupation-name",
                                                                        :definition "Tapetvalsgravör",
                                                                        :preferred-label "Tapetvalsgravör"},
                                  :version -1,
                                  :comment "Aktuella annonser saknas. Yrkesbenämning hänvisas inte. Uppdatering enligt egen utredning."}] (map gdf/extract-latest-version))]
      (is (= '[{:definition "Tapetvalsgravör",
                :id "wwEt_ceM_Uj8",
                :preferred-label "Tapetvalsgravör",
                :type "occupation-name"}] result)))))
