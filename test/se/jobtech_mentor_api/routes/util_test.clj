(ns se.jobtech-mentor-api.routes.util-test
  (:require [clojure.test :refer [deftest is testing]]
            [se.jobtech-mentor-api.routes.util :as util]))

(deftest redact-test
  (let [message {:a 1 :b 2}]
    (testing "Redactions should work"
      (is (= (util/redact-message message {:a 'redacted}) {:a 'redacted :b 2})))))
