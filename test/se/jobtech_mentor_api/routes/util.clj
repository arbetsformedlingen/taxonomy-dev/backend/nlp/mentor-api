(ns se.jobtech-mentor-api.routes.util)

(defn fold [s f d]
  (if (empty? d)
    s
    (fold (f s (first d)) f (rest d))))

(defn redact-message
  ([message] (redact-message message {:ring.logger/ms 'measured-time}))
  ([message redactions]
   (fold message
         (fn [s r]
           (if (contains? s (first r))
             (assoc s (first r) (second r))
             s)) redactions)))

(defn test-logger-pair []
  (let [log-atom (atom [])
        log-fn (fn [{:keys [level throwable message]}]
                 (let [redacted-message (redact-message message)]
                   (swap! log-atom conj [level throwable redacted-message])))]
    {:log log-atom :log-fn log-fn}))

(def cfg {:port 3000
          :git-login "mentor-taxonomy"
          :git-password "<SET AS ENV VARIABLE>"
          :api-private-key "testing-api-key"
          :jobtech-taxonomy-api-key "testing-api-key"
          :default-git-repo-path "test/data/annotations"})

