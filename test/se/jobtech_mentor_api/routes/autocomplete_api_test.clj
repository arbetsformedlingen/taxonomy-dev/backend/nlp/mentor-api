(ns se.jobtech-mentor-api.routes.autocomplete-api-test
  (:require [cheshire.core :as json]
            [clojure.test :refer [deftest is testing]]
            [reitit.ring :as ring]
            [ring.logger :as logger]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.mock.request :refer [request]]
            [se.jobtech-mentor-api.handler :as handler]
            [se.jobtech-mentor-api.routes.autocomplete-api :as aa]
            [se.jobtech-mentor-api.routes.util :refer [test-logger-pair cfg]]))

(deftest test-logger-test
  (testing "Context of the test assertions"
    (let [test-logger (test-logger-pair)]
      ((:log-fn test-logger) {:level "one" :throwable false :message {:ring.logger/ms 32.9}})
      (is (= @(:log test-logger) '(["one" false {:ring.logger/ms measured-time}]))))))

(defn- app-with [test-logger-atom]
  (ring/ring-handler
   (ring/router
    [aa/autocomplete-api]
    {:data
     {:middleware
      [;; Modify to spill the beans
       wrap-params
       wrap-keyword-params
       #(logger/wrap-with-logger % test-logger-atom)]}})))

(def app (#'handler/app cfg))

(defn body [response]
  (json/parse-string
   (slurp (:body response))
   true))

(deftest smart-0
  (testing "GET /autocomplete/smart?type=my-type&q=a%20query"
    (let [response (app (request :get "/autocomplete/smart" {:type "my-type"}))
          status (response :status)
          body (body response)]
      (is (= 400 status))
      (is (= {:q ["missing required key"]} (:humanized body)))
      (is (= {:type "my-type"} (:value body))))))

(deftest smart-1
  (testing "GET /autocomplete/smart?type=my-type&q=a%20query"
    (let [response (app (request :get "/autocomplete/smart" {:q ""
                                                             :limit 0}))
          status (response :status)
          body (body response)]
      (is (= 400 status))
      (is (= {:humanized {:limit ["should be at least 1"]}
              :value {:limit "0"
                      :q ""}} body)))))

(deftest smart-2
  (testing "GET /autocomplete/smart?type=my-type&q=a%20query"
    (let [response (app (request :get "/autocomplete/smart" {:q ""
                                                             :limit "9460993"}))
          status (response :status)
          body (body response)]
      (is (= 400 status))
      (is (= {:humanized {:limit ["should be at most 100"]},
              :value {:limit "9460993"
                      :q ""}} body)))))

(deftest smart-3
  (testing "GET /autocomplete/smart?type=my-type&q=a%20query"
    (let [response (app (request :get "/autocomplete/smart" {:q "a query"
                                                             :type "my-type"}))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (uuid? (parse-uuid (:feedback-key body))))
      (is (sequential? (body :concepts)))
      (is (= {:alternative-labels ["Director of photography" "DoP" "Filmfotograf"]
              :id "ud3i_sVp_QPh"
              :preferred-label "Director of photography/DoP/Filmfotograf"
              :type "occupation-name"} (first (:concepts body)))))))

(deftest simple-valid-request-0
  (let [response (app (request :get "/autocomplete/simple"))
        status (:status response)
        body (body response)] 
    (testing "Are we OK?"
      (is (= 400 status)))
    (testing "q was null"
      (is (= {:q ["missing required key"]} (:humanized body))))
    (is (= {} (:value body)))))

(deftest simple-valid-request-1
  (let [response (app (request :get "/autocomplete/simple?type=my-type&q=a%20query"))
        status (response :status)
        body (body response)]
    (is (uuid? (parse-uuid (:feedback-key body))))
    (testing "Are we OK?"
      (is (= 200 status)))
    (testing "And a list of concepts?"
      (is (sequential? (:concepts body))))
    (is (= [{:id "yXg3_YoW_xeC"
             :preferred-label
             "Clojure, programmeringsspråk",
             :type "skill"}] (:concepts body)))))

(deftest feedback-submit-0
  (let [logger-pair (test-logger-pair)
        response ((app-with logger-pair)
                  (request :get "/autocomplete/feedback"
                           {:completion "my selection"}))
        status (response :status)]
    (testing "Are we OK?"
      (is (= 200 status)))
    (testing "Check that body is empty"
      (is (nil? (response :body))))
    (testing "Check the log"
      (is (= 3 (count @(:log logger-pair))))
      (let [[first-entry second-entry third-entry] @(:log logger-pair)]
        (is (= [:info
                nil
                {:request-method :get
                 :server-name "localhost"
                 :uri "/autocomplete/feedback"
                 :ring.logger/type :starting}] first-entry))
        (is (= [:debug
                nil
                {:request-method :get
                 :server-name "localhost"
                 :uri "/autocomplete/feedback"
                 :ring.logger/type :params
                 :params {:completion "my selection"}}] second-entry))
        (is (= [:info
                nil
                {:request-method :get
                 :server-name "localhost"
                 :uri "/autocomplete/feedback"
                 :ring.logger/type :finish
                 :status 200
                 :ring.logger/ms 'measured-time}] third-entry))))))
