(ns se.jobtech-mentor-api.routes.core-schema-test
  (:require [clojure.test :refer [deftest is testing]]
            [malli.core :as mc]
            [peridot.multipart]
            [se.jobtech-mentor-api.routes.services :as c]))

(deftest test-types-schema
  (testing "types schema"
    (let [types [:map c/types-schema]
          schema (mc/schema types)]
      (is (mc/schema? schema))
      (is (mc/validate schema {:types #{"skill", "occupation-name"}})))))

(deftest test-types-coercion
  (testing "types coercion"
    (let [types [:map c/types-schema]
          schema (mc/schema types)]
      (is (mc/schema? schema))
      (is (mc/validate schema {:types #{"skill", "occupation-name"}})))))

(deftest test-types-co
  (testing "types coercion"
    (let [types [:map c/types-schema]
          schema (mc/schema types)]
      (is (mc/schema? schema))
      (is (mc/validate schema {:types #{"skill", "occupation-name"}})))))

(deftest test-malli-experiment
  (testing "malli experiment"
    (let [types [:map [:types :string]]
          schema (mc/schema types)]
      (is (mc/schema? schema))
      (is (mc/validate schema {:types "skill"})))))


