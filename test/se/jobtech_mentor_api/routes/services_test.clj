(ns se.jobtech-mentor-api.routes.services-test
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.test :refer [deftest is testing]]
            [peridot.multipart]
            [ring.mock.request :refer [json-body request]]
            [se.jobtech-mentor-api.handler :as handler]
            [se.jobtech-mentor-api.routes.services :as services]
            [se.jobtech-mentor-api.routes.util :refer [cfg]]))

(def app-end-point (#'handler/app cfg))

(defn body [response]
  (json/parse-string
   (slurp (:body response))
   true))

;;files
(deftest test-upload-education-description
  (testing "POST /files/upload-education-description"
    (let [response (-> (ring.mock.request/request :post "/files/upload-education-description")
                       (merge (peridot.multipart/build {:file (io/as-file "test/se/jobtech_mentor_api/routes/test.txt")}))
                       app-end-point)
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= {:annotations []
              :sha1 "d68b55f58762f347feef73712c7dfa478940aab3"
              :text "Use this file for test upload-education-description.\n\n"} body)))))

;;Main
(deftest test-main-recommender
  (testing "POST /main/recommender"
    (let [response (-> (request :post "/main/recommender")
                       (json-body {:ids ["n1YH_eKo_mvR"]
                                   :types ["sni-level-4"]
                                   :version 5})
                       app-end-point)
          status (response :status)]
      (is (= 200 status)))))

;;Autocomplete
(deftest test-autocomplete-feedback
  (let [response (app-end-point
                  (request :get "/autocomplete/feedback"
                           {:completion "my feedback"}))
        status (response :status)]
    (testing "Are we OK?"
      (is (= 200 status)))
    (testing "Check that body is empty"
      (is (nil? (response :body))))))

(deftest test-autocomplete-simple
  (testing "GET /autocomplete/simple"
    (let [response (app-end-point (request :get "/autocomplete/simple" {:type "my-type"
                                                                        :q "my-query"}))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= [{:id "yXg3_YoW_xeC",
               :preferred-label "Clojure, programmeringsspråk",
               :type "skill"}] (:concepts body))))))

(deftest test-autocomplete-smart
  (testing "GET /autocomplete/smart"
    (let [response (app-end-point (request :get "/autocomplete/smart" {:q "malmö"}))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= [{:id "oYPt_yRA_Smm",
               :preferred-label "Malmö",
               :type "municipality"}] (:concepts body))))))

(deftest test-swedish-regions
  (testing "GET /autocomplete/swedish-regions"
    (let [response (app-end-point (request :get "/autocomplete/swedish-regions" {:types "my-type"
                                                                                 :q "my-query"}))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= [{:id "yXg3_YoW_xeC",
               :preferred-label "Clojure, programmeringsspråk",
               :type "skill"}] (:concepts body))))))

;;NLP
(deftest test-education-description
  (testing "POST /nlp/education-description"
    (let [response (-> (request :post "/nlp/education-description")
                       (json-body {:text "ux-designer"
                                   :types ["occupation-name"]})
                       app-end-point)
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= {:annotations [{:annotation-id 0,
                             :concept-id "iwJU_gW4_SxT",
                             :end-position 11,
                             :matched-string "ux-designer",
                             :preferred-label "UX-designer",
                             :sentiment "OFFERED",
                             :start-position 0,
                             :type "occupation-name"}],
              :sha1 "c43c72dde5befebb6b58c8c32b894a548d339dda",
              :text "ux-designer"} body)))))
;;This test need valid id from job ad to get 200
(deftest test-job-ad
  (testing "GET /nlp/job-ad"
    (let [response (app-end-point (request :get "/nlp/job-ad" {:id "28288197"
                                                               :type "skill"}))
          status (response :status)]
      (is (#{200 404} status)))))

(deftest test-sentiment-types
  (testing "GET /nlp/sentiment-types"
    (let [response (app-end-point (request :get "/nlp/sentiment-types" {:closed false}))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= '({:id "MUST_HAVE", :swedish-label "Krav"}
               {:id "NICE_TO_HAVE", :swedish-label "Meriterande"}
               {:id "OFFERED", :swedish-label "Erbjudande"}
               {:id "NOT_RELEVANT", :swedish-label "Inte relevant"}) body)))))

(deftest test-simple-text-to-concepts
  (testing "POST /nlp/simple-text-to-concepts"
    (let [response (-> (request :post "/nlp/simple-text-to-concepts")
                       (json-body {:text "ux-designer"
                                   :types ["occupation-name"]})
                       app-end-point)
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= '({:id "iwJU_gW4_SxT",
                :preferred-label "UX-designer",
                :type "occupation-name"}) body)))))

;;private
(deftest test-private-login
  (testing "GET /private/login"
    (let [response (app-end-point (request :get "/private/login" {:api-key (:api-private-key cfg)}))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= {:response "You are logged in"} body)))))

(deftest test-save-annotated-text
  (testing "POST /private/save-annotated-text"
    (let [response (app-end-point (-> (request :post "/private/save-annotated-text")
                                      (json-body {:annotated-doc
                                                  {:meta {} :text "my-text" :title "my-title" :sha1 "my-sha1",
                                                   :annotations [{:annotation-id 0 :matched-string "my-matched" :start-position 0 :end-position 0 :comment "my-comment"
                                                                  :sentiment "my-sentiment" :concept-id "gjd2_JSR_AeG" :preferred-label "Hovslageri" :type "skill"}]}
                                                  :api-key (:api-private-key cfg)})))
          status (response :status)
          body (body response)]
      (is (= 200 status))
      (is (= {:response "Something went wrong!"} body)))))

(deftest anyway-the-main-thing-is-0
  (let [response (app-end-point (request :get "/swagger.json"))]
    (testing "GET /swagger.json"
      (is (= 200 (response :status))))
    (testing "GET /swagger.json"
      (is (s/includes?
           (-> response :body slurp)
           "\"swagger\":\"2.0\"")))))

(deftest anyway-the-main-thing-is-1
  (testing "GET /autocomplete/smart?type=my-type&q=a%20query"
    (let [response (app-end-point (request :get "/autocomplete/smart" {:type "my-type"
                                                                       :q "a query"}))
          status (response :status)
          body (first ((body response) :concepts))]
      (is (= 200 status))
      (is (= {:alternative-labels ["Director of photography" "DoP" "Filmfotograf"],
              :id "ud3i_sVp_QPh",
              :preferred-label "Director of photography/DoP/Filmfotograf",
              :type "occupation-name"} body)))))

(deftest test-csv-to-set
  (testing "Test csv to set"
    (is (= #{"skill" "occupation-name" "language"} (services/csv-to-set "skill, occupation-name, language")))
    (is (= #{"skill" "occupation-name" "language"} (services/csv-to-set "skill,occupation-name,language")))
    (is (= #{"skill" "language"} (services/csv-to-set "skill,,language")))
    (is (= #{"skill"} (services/csv-to-set "skill")))
    (is (= #{} (services/csv-to-set "")))
    (is (nil? (services/csv-to-set nil)))))

(deftest test-find-concepts-in-text-happy-path
  (testing "Find concepts in text happy path"
    (let [result (services/find-concepts-in-text-simple "ux-designer" #{"occupation-name"})]
      (is (= [{:preferred-label "UX-designer",
               :type "occupation-name",
               :id "iwJU_gW4_SxT"}] result)))))

(deftest test-concepts-lookup
  (testing "concept lookup should not explode, test added with
            bugfix Nullpointer Simple text to concepts , #54"
    (is (> (count (services/concepts-lookup #{"keyword" "occupation-name" "skill" "language"}))
           5))))

(deftest test-lookup-concept
  (testing "lookup concept"
    (is (>= (count (services/lookup-concept "ux-designer" #{"occupation-name"}))
            1))))







