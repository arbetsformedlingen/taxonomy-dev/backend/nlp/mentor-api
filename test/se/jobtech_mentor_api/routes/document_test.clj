(ns se.jobtech-mentor-api.routes.document-test
  (:require [clojure.test :refer [deftest testing is]]
            [malli.core :as ma]
            [se.jobtech-mentor-api.routes.document :as doc]))

(deftest concept-1
  (testing "Check that an id can be a string value"
    (is (ma/validate
         doc/concept-spec
         {:id "Some Text ID"
          :type "My Type"
          :preferred-label "No Name"}))))

(deftest annotation-1
  (testing "Check that we can create an empty single concept annotation"
    (is (ma/validate
         doc/single-concept-annotation-spec
         {:annotation-id nil
          :matched-string ""
          :start-position 0
          :end-position 1
          ;:comment nil
          ;:sentiment nil
          :concept-id nil
          :preferred-label nil
          :type nil}))))

(deftest annotation-2
  (testing "Check that we can create an empty single concept annotation"
    (is (ma/validate
         doc/single-concept-annotation-spec
         {:annotation-id nil
          :matched-string ""
          :start-position 0
          :end-position 1
          ;:comment nil
          ;:sentiment nil
          :concept-id nil
          :preferred-label nil
          :type nil}))))

(deftest annotation-3
  (testing "Check that we can create an empty multiple concept annotation"
    (is (ma/validate
         doc/multiple-concept-annotation-spec
         {:annotation-id nil
          :matched-string ""
          :start-position 0
          :end-position 1
          ;:comment nil
          ;:sentiment nil
          :concepts []}))))

(deftest annotation-4
  (testing "Check that we can create an empty single concept annotation"
    (is (ma/validate
         doc/annotation-spec
         {:annotation-id nil
          :matched-string ""
          :start-position 0
          :end-position 1
          ;:comment nil
          ;:sentiment nil
          :concept-id nil
          :preferred-label nil
          :type nil}))))

(deftest annotation-5
  (testing "Check that we can create an empty multiple concept annotation"
    (is (ma/validate
         doc/annotation-spec
         {:annotation-id nil
          :matched-string ""
          :start-position 0
          :end-position 1
          ;:comment nil
          ;:sentiment nil
          :concepts []}))))

(deftest annotation-fail-1
  (testing "Check that we cannot create an empty single and multiple concept annotation"
    (is (not (ma/validate
              doc/annotation-spec
              {:annotation-id nil
               :matched-string ""
               :start-position 0
               :end-position 1
          ;:comment nil
          ;:sentiment nil
               :concepts []
               :concept-id nil
               :preferred-label nil
               :type nil})))))