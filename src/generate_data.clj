(ns generate-data
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]
            [clojure.string :as st]
            [clojure.walk :as walk]
            [se.jobtech-mentor-api.config :refer [get-config]]
            [se.jobtech-mentor-api.generate-data-functions :as gdf]))

(def last-published-data (->> (get-in (client/get "https://data.jobtechdev.se/taxonomy/alla-koncept.json" {:as :json})
                                      [:body :data :concepts])
                              (walk/postwalk
                               (fn [x]
                                 {:pre [(or (not (keyword? x)) (nil? (namespace x)))]}
                                 (if (keyword? x)
                                   (-> x
                                       name
                                       (st/replace #"_", "-")
                                       keyword)
                                   x)))))

(defn main-versions []
  (get-in (client/get "https://api-jobtech-taxonomy-api-prod-write.prod.services.jtech.se/v1/taxonomy/main/versions" {:as :json})
          [:body]))

(def last-version-taxonomy (->> (last (main-versions))
                                ((fn [m] (-> m (get-in [:taxonomy/version]))))))

(defn concepts-from-prod-write []
  (->> (client/get "https://api-jobtech-taxonomy-api-prod-write.prod.services.jtech.se/v1/taxonomy/private/concept/changes?"
                   {:query-params {"after-version" last-version-taxonomy}
                    :headers {:api-key (get-in (get-config []) [:jobtech-taxonomy-api-key])}
                    :as :json}) :body))

(def data (gdf/merge-datas last-published-data (concepts-from-prod-write)))

(defn write-data-to-file []
  (spit "src/se/jobtech_mentor_api/routes/data.json" (json/write-str data)))

(write-data-to-file)

#_(println (slurp "src/clj/jobtech_mentor_api/routes/data.json"))


