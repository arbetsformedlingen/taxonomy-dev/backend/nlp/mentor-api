(ns se.jobtech-mentor-api.core
  (:gen-class)
  (:require reitit.coercion.malli
            reitit.ring.malli
            [ring.adapter.jetty :as jetty]
            [se.jobtech-mentor-api.config :refer [get-config]]
            [se.jobtech-mentor-api.handler :refer [app]]))

(def default-app
  (app (get-config [])))

(defn start
  ([] (start (get-config [])))
  ([config] (->> (jetty/run-jetty (app config) {:port (get-in config [:port]) :join? false})
                 (do (println "server running in port" (get-in config [:port]))))))

;; Start developement with :dev profile and (dev-start) from the user namespace
(defn -main [& args]
  (print "Running main function")
  (start (get-config args)))
