(ns se.jobtech-mentor-api.generate-data-functions)

(defn remove-taxonomy-string [m] (update-keys m (fn [k] (keyword (name k)))))

(defn extract-latest-version [m] (-> m :taxonomy/latest-version-of-concept remove-taxonomy-string))

(defn merge-datas [published-data unpublished-data]
  (distinct (flatten (seq (merge published-data (->> unpublished-data (map extract-latest-version)))))))
