(ns se.jobtech-mentor-api.routes.store
  (:require [cheshire.core :as cheshire]
            [clj-commons.digest :as digest]
            [clj-jgit.porcelain :as git]
            [clojure.java.io :as jio]
            [clojure.pprint :as pp]
            [clojure.tools.logging :as log]
            [se.jobtech-mentor-api.config :refer [get-config]]))

(defn- create-directory [git-repo-path]
  (if (not (.exists (java.io.File. git-repo-path)))
    (do (.mkdir (java.io.File. git-repo-path))
        (println "Creating temporary folder: " git-repo-path))
    (println git-repo-path "already exists")))

(defn create-git-repo []
  (let [config (get-config [])]
    (try
      (create-directory (:default-git-repo-path config))
      (git/load-repo (:default-git-repo-path config))
      (catch Exception _e
        (git/git-clone (:git-repo-url config)
                       :dir (:default-git-repo-path config))))))

(defn save-annotation-data-to-disk! [annotation-doc config]
  (try
    (let [annotation-text-sha1 (digest/sha-1 (annotation-doc :text))
          filename (str annotation-text-sha1 ".json")
          save-location (str (:default-git-repo-path config) "/" filename)
          annotation-doc (assoc annotation-doc :sha1 annotation-text-sha1)]
      (with-open [w (jio/writer (str (:default-git-repo-path config) "/" filename))]
        (cheshire/generate-stream annotation-doc w {:pretty true}))
      (git/with-credentials {:login (:git-login config)
                             :pw (:git-password config)}

        (git/git-add create-git-repo filename)
        (git/git-commit create-git-repo (str "add file " filename) :committer {:name "annotations bot " :email "annotations-bot@jobtechdev.se"})
        (git/git-push create-git-repo)
        (log/info :save-location save-location)))
    (catch Exception e
      (pp/pprint e))))
