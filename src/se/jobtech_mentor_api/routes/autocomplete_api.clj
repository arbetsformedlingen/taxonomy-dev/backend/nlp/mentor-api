(ns se.jobtech-mentor-api.routes.autocomplete-api
  (:gen-class)
  (:require
   [clojure.string :as st]
   [clojure.tools.logging :as ctl]
   [jobtechdev.taxonomy.autocomplete.core :as ac]
   [se.jobtech-mentor-api.middleware.cors :as cors]))

(def autocomplete-concept-spec
  [:map
   [:id :string]
   [:type {:optional true} :string]
   [:preferred-label :string]
   [:alternative-labels {:optional true} [:sequential :string]]
   [:hidden-labels {:optional true} [:sequential :string]]])

(def ^:private autocomplete-api-responses
  {200 {:body [:map [:feedback-key :uuid]
               [:concepts [:sequential autocomplete-concept-spec]]]}})

(defn dummy-autocomplete-handler
  [{{{:keys [q]} :query} :parameters}]
  (when (string? q)
    {:status 200
     :body
     {:feedback-key (random-uuid)
      :concepts [{:id "yXg3_YoW_xeC"
                  :preferred-label "Clojure, programmeringsspråk"
                  :type "skill"}]}}))

(defn remove-namespace-from-keywords [concepts]
  (map #(update-keys % (comp keyword name)) concepts))

(defn- smart-autocomplete-handler
  [{{{:keys [q types limit]} :query} :parameters}]
  (when (string? q)
    {:status 200
     :body
     {:feedback-key (random-uuid)
      :concepts (remove-namespace-from-keywords
                 (ac/autocomplete {:query-string q
                                   :type types
                                   :limit limit}))}}))

(def autocomplete-api
  ["/autocomplete"
   {:swagger {:tags ["Autocomplete"]}
    :middleware [cors/cors]}
   ["/feedback"
    {:get
     {:summary "Completion feedback"
      :parameters {:query [:map [:completion :string]]}
      :responses {200 {:body :any}}
      :handler (fn [{{{:keys [completion]} :query} :parameters}]
                 (ctl/info "/autocomplete/feedback" "{" :completion (str "\"" completion "\"") "}")
                 (comment
                   "This should log the feedback completion.")
                 {:status 200 :body nil})}}]
   ["/simple"
    {:get
     {:summary "Simple autocomplete"
      :parameters {:query [:map
                           [:types {:optional true} :string]
                           [:q :string]]}
      :responses autocomplete-api-responses
      :handler dummy-autocomplete-handler}}]

   ["/smart"
    {:get
     {:summary "Smart autocomplete"
      :parameters {:query [:map
                           [:q :string]
                           [:types {:optional true
                                    :decode/string (fn [s] (st/split s #","))}
                            [:sequential [:string]]]
                           [:limit {:optional true} [:int {:min 1, :max 100}]]]}
      :responses autocomplete-api-responses
      :handler smart-autocomplete-handler}}]

   ["/swedish-regions"
    {:get
     {:summary "Swedish regions autocomplete"
      :parameters {:query [:map [:types :string] [:q :string]]}
      :responses autocomplete-api-responses
      :handler dummy-autocomplete-handler}}]])
