(ns se.jobtech-mentor-api.routes.document)

;;;; SPEC
(def metadata-spec [:map {:closed false}])

(def concept-spec
  [:map
   [:id :string]
   [:type :string]
   [:preferred-label :string]])

(def multiple-concept-annotation-spec
  [:map {:closed true}
   [:annotation-id [:maybe :int]]
   [:matched-string [:maybe :string]]
   [:start-position :int]
   [:end-position :int]
   [:comment {:optional true} :string]
   [:sentiment {:optional true} :string]
   [:concepts [:sequential concept-spec]]])

(def single-concept-annotation-spec
  [:map {:closed true}
   [:annotation-id [:maybe :int]]
   [:matched-string :string]
   [:start-position :int]
   [:end-position :int]
   [:comment {:optional true} :string]
   [:sentiment {:optional true} :string]
   [:concept-id [:maybe :string]]
   [:preferred-label [:maybe :string]]
   [:type [:maybe :string]]])

(def annotation-spec
  [:or
   single-concept-annotation-spec
   multiple-concept-annotation-spec])

(def annotations-spec [:map
                       [:meta {:optional true} metadata-spec]
                       [:text :string]
                       [:title {:optional true} :string]
                       [:sha1 :string]
                       [:annotations [:sequential annotation-spec]]])

(def concept-with-affinity-spec
  [:map
   [:id :string]
   [:type :string]
   [:preferred-label :string]
   [:definition :string]
   [:affinity :int]])
