(ns se.jobtech-mentor-api.routes.services
  (:gen-class)
  (:require [clj-commons.digest :as digest]
            [clj-http.client :as client]
            [clojure.data.json :as json]
            [clojure.set :as se]
            [clojure.string :as st]
            [pantomime.extract :as extract]
            reitit.coercion.malli
            reitit.ring.malli
            [reitit.swagger :as swagger]
            [se.jobtech-mentor-api.middleware.cors :as cors]
            [se.jobtech-mentor-api.routes.document :refer [annotations-spec concept-spec
                                                           concept-with-affinity-spec]]
            [se.jobtech-mentor-api.routes.store :refer [save-annotation-data-to-disk!]]))

;; Config

;; Uncomment to use and put in require
;; [reitit.ring.middleware.dev :as dev]
;; [reitit.ring.spec :as spec]
;; [spec-tools.spell :as spell]

(defn clean-concept [{:keys [id preferred-label type]}]
  {:concept-id id
   :preferred-label preferred-label
   :type type})

(def regex-char-esc-smap
  (let [esc-chars "(){}[]&^%$#!?*.+"]
    (zipmap esc-chars
            (map #(str "\\" %) esc-chars))))

(defn escape-regexp [s]
  (->> s (replace regex-char-esc-smap) (reduce str)))

(defn normalize-string [s]
  (st/lower-case (st/trim s)))

(defn clean-string [s]
  (-> s
      normalize-string
      escape-regexp))

(defn read-data [data] (lazy-seq (json/read-str (slurp data) :key-fn keyword)))

(def memoized-all-concepts (memoize read-data))

;;  #{"esco-skill"}
(defn concepts-by-types [types]
  (filter #(contains? types (:type %)) (memoized-all-concepts "src/se/jobtech_mentor_api/routes/data.json")))

(defn lookup-reducer [acc element]
  (let [acc1 (assoc acc (st/lower-case (:preferred-label element))
                    (clean-concept element))]
    (reduce (fn [a e]
              (assoc a (st/lower-case e)
                     (clean-concept element))) acc1 (:alternative_labels element))))

;; filter on types
(def memoized-concepts
  (memoize (fn [types] (concepts-by-types types))))

(defn concepts-lookup [types]
  (reduce lookup-reducer {}
          (memoized-concepts types)))

;; Fixa look up!!! and rename stuff to make sense
(def memoized-concept-lookup (memoize (fn [types]
                                        (concepts-lookup types))))

;; (lookup-concept "clojure" #{"skill"})
(defn lookup-concept [label types]
  (get (memoized-concept-lookup types) label))

(def concept-types #{"keyword"
                     "occupation-name"
                     "language"
                     "occupation-field"
                     "skill",
                     "skill-headline",
                     "sni-level-1",
                     "sni-level-2",
                     "sni-level-3",
                     "sni-level-4",
                     "sni-level-5",
                     "ssyk-level-1",
                     "ssyk-level-2",
                     "ssyk-level-3",
                     "ssyk-level-4",
                     "sun-education-field-1",
                     "sun-education-field-2",
                     "sun-education-field-3",
                     "sun-education-field-4",
                     "sun-education-level-1",
                     "sun-education-level-2",
                     "sun-education-level-3"})

(def stop-words #{"Utbildning" "Utbildningsledare"
                  "Tjänster" "Rektor" "Nätverk" "Vi" "vi"
                  "positiv" "Positiv"})

(defn filter-valid-concepts [concept]
  (and
   (< 3 (count (:preferred-label concept)))
   (not (contains? stop-words (:preferred-label concept)))
   (empty? (se/intersection (set (:alternative_labels concept)) stop-words))))

(defn extract-labels [concept]
  (remove st/blank? (cons (:preferred-label concept) (:alternative_labels concept))))

(defn clean-concepts [types]
  (filter filter-valid-concepts (memoized-concepts types)))

(defn pad-with-regexp-b [s]
  (str "\\b" s "\\b"))

(defn all-concepts-regexp-string [types]
  (let [concepts (clean-concepts types)
        labels (mapcat extract-labels concepts)
        normalized-labels (map clean-string labels)]
    (str (st/join "|" (map pad-with-regexp-b normalized-labels)) "(?i)")))

(defn find-labels-in-text [text types]
  (let [matcher (re-matcher (re-pattern (all-concepts-regexp-string types)) (st/lower-case text))
        matches ((fn step []
                   (when-let [match (re-find matcher)]
                     (cons {:start-position (.start matcher)
                            :end-position (.end matcher)
                            :matched-string (st/lower-case match)} (lazy-seq (step))))))]
    matches))

(defn find-concepts-in-text [text text-type types]
  (reduce (fn [acc element]
            (if-let [concept (get (memoized-concept-lookup types) (:matched-string element))]
              (cons (merge concept element {:annotation-id (count acc)
                                            :sentiment (if (= text-type :jobad)
                                                         "MUST_HAVE"
                                                         "OFFERED")}) acc)
              acc)) [] (find-labels-in-text text types)))

(defn default-types-if-empty [types]
  (if (empty? types)
    (set concept-types)
    types))

(defn annotate-text [text text-type types]
  {:text text
   :sha1 (digest/sha-1 text)
   :annotations (find-concepts-in-text text text-type (default-types-if-empty types))})

(defn recommend-by-ids [_]
  [])

(defn fetch-job-ad [id]
  (let [response (client/get
                  (format "https://jobsearch.api.jobtechdev.se/ad/%s" id)
                  {:throw-exceptions false :as :json})]
    (if (:status 200)
      {:status 200 :body (:body response)}
      {:status (:status response) :body (:body response)})))

(defn annotate-jobad [ad types]
  (let [headline (get-in ad [:headline])
        text (str headline "\n" (get-in ad [:description :text]))
        annotated-text (annotate-text text :jobad types)
        annotated-text-with-ad-metadata (assoc-in annotated-text [:meta :jobad] ad)]
    (assoc-in annotated-text-with-ad-metadata [:meta :name] headline)))

(defn strip-namespace [{:concept/keys [id type definition preferred-label]
                        affinity :cosine-similarity}]
  {:id id
   :type type
   :definition definition
   :preferred-label preferred-label
   :affinity affinity})

(defn get-private-api-key-from [request]
  (get-in request [:mentor-api :config :api-private-key]))

(defn get-api-key-from [request]
  (get-in request [:parameters :query :api-key]))

;; See this for inspiration https://github.com/lipas-liikuntapaikat/lipas/blob/master/webapp/src/clj/lipas/backend/middleware.clj
(defn swagger-api []
  ["/swagger.json"
   {:get {:no-doc true
          :swagger {:info {:title "Mentor API"
                           :description "Helping out with using the Jobtechdev Taxonomy"}
                    :tags [{:name "files", :description "file api"}]}
          :handler (swagger/create-swagger-handler)}}])

(defn mentor-api-main []
  ["/main" {:swagger {:tags ["Main"]}
            :middleware [cors/cors]}
   ["/recommender"
    {:post {:summary "Get related concepts recommendations to a list of concept ids"
            :parameters {:body [:map
                                [:ids {:json-schema/example ["n1YH_eKo_mvR", "GLJP_J7o_Nmw"]} [:sequential :string]]
                                [:types {:optional true :json-schema/example ["sni-level-4", "sni-level-5"]} [:sequential :string]]
                                [:version {:json-schema/example 5} :int]]}
            :responses {200 {:body [:map [:concepts [:sequential concept-with-affinity-spec]]]}}
            :handler (fn [{{{:keys [ids types _version]} :body}
                           :parameters}]
                       {:status 200
                        :body {:concepts (into []
                                               (comp (map strip-namespace)
                                                     (if (empty? types)
                                                       identity
                                                       (filter (comp (set types) :type))))
                                               (recommend-by-ids ids))}})}}]])

(defn find-concepts-in-text-simple [text types]
  (let [ts (default-types-if-empty types)]
    (remove nil? (map #(se/rename-keys % {:concept-id :id})
                      (set (map #(lookup-concept (:matched-string %) ts)
                                (find-labels-in-text text ts)))))))

(defn csv-to-set [s] (when (string? s)
                       (into #{} (comp (map st/trim)
                                       (filter (comp not st/blank?))) (st/split s #","))))

(def types-schema
  [:types {:optional true
           :decode/string csv-to-set}
   [:set {:json-schema/example
          "skill, occupation-name, keyword, language etc."} [:string]]])

(defn nlp-api []
  ["/nlp"
   {:swagger {:tags ["NLP"]}
    :middleware [cors/cors]}

   ["/simple-text-to-concepts"
    {:post
     {:summary "Extract taxonomy concepts from text"
      :parameters {:body [:map
                          [:text :string]
                          types-schema]}
      :responses {200 {:body [:sequential concept-spec]}}
      :handler (fn [{{{:keys [text types]} :body} :parameters}]
                 {:status 200
                  :body (find-concepts-in-text-simple text types)})}}]

   ["/education-description"
    {:post
     {:summary "Annotate education description"
      :parameters {:body [:map [:text :string]
                          types-schema]}
      :responses {200 {:body annotations-spec}}
      :handler (fn [{{{:keys [text types]} :body} :parameters}]
                 {:status 200
                  :body (annotate-text text :education-description types)})}}]

   ["/job-ad"
    {:get
     {:summary "Annotate job ad"
      :parameters {:query [:map
                           [:id :string]
                           types-schema]}
      :responses {200 {:body annotations-spec}}
      :handler (fn [{{{:keys [id types]} :query} :parameters}]
                 (let [ad (fetch-job-ad id)]
                   (if (= 200 (:status ad))
                     {:status 200 :body (annotate-jobad (:body ad) types)}
                     ad)))}}]

   ["/sentiment-types"
    {:get
     {:summary "List of sentiments"
      :responses {200 {:body [:sequential [:map {:closed false}]]}}
      :handler (fn [_]
                 {:status 200
                  :body
                  [{:id "MUST_HAVE" :swedish-label "Krav"}
                   {:id "NICE_TO_HAVE" :swedish-label "Meriterande"}
                   {:id "OFFERED" :swedish-label "Erbjudande"}
                   {:id "NOT_RELEVANT" :swedish-label "Inte relevant"}]})}}]])

(defn files-api []
  ["/files"
   {:swagger {:tags ["files"]}
    :middleware [cors/cors]}

   ["/upload-education-description"
    {:post {:summary "upload a file"
            :parameters {:multipart [:map [:file reitit.ring.malli/temp-file-part] types-schema]}
            :responses {200 {:body annotations-spec}}
            :handler (fn [{{{:keys [file types]} :multipart} :parameters}]
                       {:status 200
                        :body (annotate-text (:text (extract/parse (:tempfile file)))
                                             :education-description types)})}}]])

(defn private-api []
  ["/private"
   {:swagger {:tags ["private"]}
    :middleware [cors/cors]}

   ["/login"
    {:get
     {:summary "use this to check if the user has access"
      :parameters {:query [:map [:api-key :string]]}
      :responses {200 {:body [:map
                              [:response :string]]}}
      :handler (fn [request]
                 (if (= (get-api-key-from request) (get-private-api-key-from request))

                   {:status 200
                    :body {:response "You are logged in"}}

                   {:status 401
                    :body {:response "Bad api key!"}}))}}]

   ["/save-annotated-text"
    {:post
     {:summary "Save annotated text"
      :parameters {:body [:map {:closed false} [:annotated-doc annotations-spec]
                          [:api-key :string]]}
      :responses {200 {:body [:map [:response :string]]}}
      :handler (fn [{{{:keys [annotated-doc api-key config]} :body} :parameters :as request}]
                 (if (= api-key (get-private-api-key-from request))

                   {:status 200
                    :body (if (save-annotation-data-to-disk! annotated-doc config)
                            {:response "Ok!"}
                            {:response "Something went wrong!"})}
                   {:status 401
                    :body {:response "Bad api key!"}}))}}]])
