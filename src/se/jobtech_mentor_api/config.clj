(ns se.jobtech-mentor-api.config
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [malli.core :as m]
            [malli.error :as me]
            [taoensso.timbre :as log]))

(def mentor-config-schema
  [:map
   [:port :int]
   [:repl {:optional true} :int]
   [:git-login :string]
   [:git-password :string]
   [:api-private-key :string]
   [:jobtech-taxonomy-api-key :string]
   [:default-git-repo-path :string]])

(defn- cli-flag->key [arg]
  (case arg
    ("--port" "-p") :port
    ("--repl" "-r") :repl
    ("--config" "-c") :config
    arg))

(defn- key+cli-value->key+value [[key cli-value]]
  (case key
    (:port :repl)
    (try
      (let [port (Integer/parseInt cli-value)]
        (if (< 0 port 0x10000)
          {:options {key port}}
          {:errors {key port}}))
      (catch java.lang.NumberFormatException _e
        {:errors {key cli-value}}))
    :config (if (.exists (io/file cli-value))
              {:options {key cli-value}}
              {:errors {key cli-value}})
    {:errors {:key key :value cli-value}}))

(defn- validate-cli [args]
  (let [invalid-keys (->>
                      (take-nth 2 args)
                      frequencies
                      (filter (fn more-than-one [[_key count]] (> count 1)))
                      (mapv first))]
    (when (not-empty invalid-keys)
      {:errors {:key-count invalid-keys}})))

(defn- parse-cli [args]
  (let [key-params (mapv cli-flag->key args)
        invalid-keys (validate-cli key-params)
        options (apply merge-with into (map key+cli-value->key+value (partition 2 key-params)))]
    (merge options invalid-keys)))

(defn cli-env-def [cli-value env-var-name env-var-parser]
  (or (when cli-value
        (log/info "Using CLI parameter for" env-var-name ":" cli-value)
        cli-value)
      (when-some [env-var (System/getenv env-var-name)]
        (log/info "Parsing" env-var-name)
        (env-var-parser env-var))))

(defn expand-home [s]
  (if (.startsWith s "~")
    (s/replace-first s "~" (System/getProperty "user.home"))
    s))

(defn- config-file-order
  "The order to look for configuration files."
  [cli-config]
  (->> [cli-config
        (System/getenv "MENTOR_CONFIG_PATH")
        "resources/mentor-api.edn"
        (expand-home "~/.jobtech/mentor-api.edn")
        "mentor-api.edn"]
       (filter some?)
       (map io/file)
       (filter #(.exists %))
       first))

(defn- load-config [cli-config]
  (try
    (when-let [config-file (config-file-order cli-config)]
      (let [config (with-open [reader (io/reader config-file)]
                     (log/debug :config/load-config config-file)
                     (edn/read (java.io.PushbackReader. reader)))]
        (assoc config :path (.getPath config-file))))
    (catch java.lang.Exception e
      (log/error "No valid configuration file found")
      (throw e))))

(defn get-config [args]
  (let [cli-opts (parse-cli args)
        {:keys [repl port config]} (:options cli-opts)]
    (if (:errors cli-opts)
      (do
        (log/error "Bad command line input" cli-opts)
        (throw (Exception. cli-opts)))
      (let [repl-port (cli-env-def repl "REPL" (fn [s] (Integer/parseInt s)))
            server-port (cli-env-def port "PORT" (fn [s] (Integer/parseInt s)))
            git-login (edn/read-string (System/getenv "GIT_LOGIN"))
            git-password (edn/read-string (System/getenv "GIT_PASSWORD"))
            api-private-key (edn/read-string (System/getenv "API_PRIVATE_KEY"))
            jobtech-taxonomy-api-key (edn/read-string (System/getenv "JOBTECH_TAXONOMY_API_KEY"))
            default-git-repo-path (edn/read-string (System/getenv "DEFAULT_GIT_REPO_PATH"))
            config (cond-> (load-config config)
                     (some? repl-port) (assoc-in [:repl] repl-port)
                     (some? server-port) (assoc-in [:port] server-port)
                     (some? git-login) (assoc-in [:git-login] git-login)
                     (some? git-password) (assoc-in [:git-password] git-password)
                     (some? api-private-key) (assoc-in [:api-private-key] api-private-key)
                     (some? jobtech-taxonomy-api-key) (assoc-in [:jobtech-taxonomy-api-key] jobtech-taxonomy-api-key)
                     (some? default-git-repo-path) (assoc-in [:default-git-repo-path] default-git-repo-path))]
        (log/debug "Config" config)
        (if (m/validate mentor-config-schema config)
          config
          (log/error "Invalid mentor configuration:" (me/humanize (m/explain mentor-config-schema config))))))))

