(ns se.jobtech-mentor-api.handler
  (:gen-class)
  (:require [malli.util :as mu]
            [muuntaja.core :as m]
            reitit.coercion.malli
            [reitit.dev.pretty :as pretty]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as coercion]
            reitit.ring.malli
            [reitit.ring.middleware.exception :as exception]
            [reitit.ring.middleware.multipart :as multipart]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [ring.logger :as logger]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.params :refer [wrap-params]]
            [se.jobtech-mentor-api.routes.autocomplete-api :refer [autocomplete-api]]
            [se.jobtech-mentor-api.routes.services :as services]))

(defn app [config]
  (let [swagger-api (services/swagger-api)
        mentor-api-main (services/mentor-api-main)
        nlp-api (services/nlp-api)
        files-api (services/files-api)
        private-api (services/private-api)]
    (ring/ring-handler
     (ring/router
      [swagger-api
       mentor-api-main
       autocomplete-api
       nlp-api
       files-api
       private-api]

      {; :reitit.middleware/transform dev/print-request-diffs ;; pretty diffs
           ; :validate spec/validate ;; enable spec validation for route data
           ; :reitit.spec/wrap spell/closed ;; strict top-level validation
       :exception pretty/exception
       :data {:coercion (reitit.coercion.malli/create
                         {;; set of keys to include in error messages
                          :error-keys #{#_:type #_:coercion #_:in #_:schema :value #_:errors :humanized #_:transformed}
                           ;; support lite syntax?
                          #_#_:lite true
                           ;; schema identity function (default: close all map schemas) 
                          :compile mu/closed-schema
                           ;; validate request & response
                          #_#_:validate true
                           ;; top-level short-circuit to disable request & response coercion
                          #_#_:enabled true
                           ;; strip-extra-keys (effects only predefined transformers)
                          :strip-extra-keys true
                           ;; add/set default values
                          :default-values true
                           ;; malli options
                          :options nil})
              :muuntaja m/instance
              :middleware [(fn inject-config [handler]
                             (fn with-config [request]
                               (handler (assoc-in request [:mentor-api :config] config))))
                               ;; Make query string params visible
                           wrap-params
                               ;; Convert them to keyword params
                           wrap-keyword-params
                               ;; Log params as DEBUG
                           #(logger/wrap-with-logger % (:log config {}))
                              ;; swagger feature
                           swagger/swagger-feature
                                ;; query-params & form-params
                           parameters/parameters-middleware
                                ;; content-negotiation
                           muuntaja/format-negotiate-middleware
                               ;; encoding response body
                           muuntaja/format-response-middleware
                              ;; exception handling
                           #_(exception/create-exception-middleware
                              {::exception/default (partial exception/wrap-log-to-console exception/default-handler)})
                           exception/exception-middleware
                                ;; decoding request body
                           muuntaja/format-request-middleware
                                ;; coercing response bodys
                           coercion/coerce-response-middleware
                                ;; coercing request parameters
                           coercion/coerce-request-middleware
                                ;; multipart
                           multipart/multipart-middleware]}})
     (ring/routes
      (swagger-ui/create-swagger-ui-handler
       {:path "/"
        :config {:validatorUrl nil
                 :operationsSorter "alpha"}})
      (ring/create-default-handler)))))

