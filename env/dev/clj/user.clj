(ns user
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.reload :refer [wrap-reload]]
            [se.jobtech-mentor-api.core :as jobtech-mentor-api]
            se.jobtech-mentor-api.routes.services))

(println "\n\n run (dev-start) to start the developement jetty server")

(def dev-app (wrap-reload #'jobtech-mentor-api/default-app))

(defn ^:export dev-start [] (->> (jetty/run-jetty dev-app {:port 3000, :join? false})
                        (do (println "server running in port 3000"))))
