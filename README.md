# Mentor API   

An API for mentoring the creation of CVs and Job Postings based on the insightfullness of the Taxonomy.


## Installation

Create a direcotry in `/tmp/` called annotations and make it into a git repo.

``` shell
cd /tmp/
mkdir annotations
cd annotations
git init
```

or clone the existing repo:

```shell
cd /tmp/
git clone git@gitlab.com:taxonomy-annotations/annotations.git
```
About deps edn

See https://tomekw.com/clojure-deps-edn-a-basic-guide/ on how to work with deps, like adding tests

## Usage

### Secrets

Copy the content of `env/config.edn` to `~/.jobtech/mentor-api.edn` and change the Git password to the password of the test user.

### Run

For production use, In the `core` namespace call `start` and go to `http://localhost:3000/`

For development use, start with the :dev profile and run (dev-start) in the user namespace


## Testing

Start by setting upp [kaocha](https://cljdoc.org/d/lambdaisland/kaocha/). It should now be possible to run all tests using `kaocha` and watch for changes using `kaocha --watch`.

## Deployment

To make a new deployment of the Mentor API backend choose action "Start build" on the mentor-api BuildConfig,
https://console-openshift-console.test.services.jtech.se/k8s/ns/mentor-api-4/buildconfigs
OpenShift will automatically deploy the new build.

The code for the Mentor API frontend is located at:
https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor

The Mentor API frontend is deployed at:
http://annotation-editor-dev-taxonomy-frontend-gitops.test.services.jtech.se/

### Dependencies

Taxonomy autocomplete
https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete

## 😀😀😀😀
