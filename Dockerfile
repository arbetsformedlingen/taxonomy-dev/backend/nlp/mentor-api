FROM clojure:temurin-21-tools-deps-1.11.3.1456-jammy

WORKDIR /app

COPY deps.edn ./
COPY resources ./resources/
COPY src ./src/

RUN clojure -A:build -P

RUN apt-get update && apt-get install -y apt-transport-https gnupg2 git wget &&\
    mkdir "/tmp/annotations" &&\
    cd "/tmp/annotations"

EXPOSE 3000

CMD clojure -M -m se.jobtech-mentor-api.core
